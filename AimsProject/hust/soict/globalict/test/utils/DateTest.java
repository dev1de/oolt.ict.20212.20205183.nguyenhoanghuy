package hust.soict.globalict.test.utils;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args){
        //the current date - default constructor
        MyDate dateTest1 = new MyDate();
        System.out.println("Test 1 : current date");
        dateTest1.print();

        //initialize date with a specific date using 3 int parameter, e.g 15-5-1950
        System.out.println("Test 2: Set the date to 15/5/1950");
        MyDate dateTest2 = new MyDate(15,5,1950);
        dateTest2.print();

        //initialize date with a specific date using a string, in the format yyyy-mm-dd, e.g 2022-12-15
        MyDate dateTest3 = new MyDate("2022-12-05");
        System.out.println("Test 3: initialize date with specific date using a string (2022-12-05) in format (yyyy-mm-dd)");
        dateTest3.print();

        //accept the date which input by user
        MyDate dateTest4 = new MyDate();
        System.out.println("Test 4: date enter by user");
        dateTest4.accept();
        dateTest4.print();

        MyDate dateTest5 = new MyDate("second", "September", "twenty one twenty one");
        System.out.println("Test 5: Set date with name, e.g \"second, September, twenty one twenty one\", print the date with format which entered by user");
        dateTest5.print();
        dateTest5.printWithFormat();

        MyDate[] sortedDates = new MyDate[]{dateTest1, dateTest2, dateTest3, dateTest4, dateTest5};
        System.out.println("Before sort:");
        for (MyDate d: sortedDates) {
            d.print();
        }
        DateUtils.sortDates(sortedDates);
        System.out.println("After sorted:");
        for (MyDate d: sortedDates) {
            d.print();
        }
    }
}
