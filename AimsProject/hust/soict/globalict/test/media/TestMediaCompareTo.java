package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;

import java.util.*;

public class TestMediaCompareTo {
    public static void main(String[] args) {
        Collection collection = new ArrayList();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc(1,"Harry Potter","Sci-fi","J.K.Rowling",2, 2.5f);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc(2, "Pendragon", "Sci-fi","D.J.Machale",1,1.5f);
        collection.add(dvd2);
        collection.add(dvd1);

        CompactDisc cd1 = new CompactDisc(3,"BLACKPINK","Pop","black pink","black pink",3.5f);
        Book b1 = new Book(4, "Co chang trai viet len cay","Romantic", List.of("Nguyen Ngoc Anh","Nguyen Ngoc Ngan"), 3.2f);

        collection.add(cd1);
        collection.add(b1);

        Iterator it = collection.iterator();
        while(it.hasNext()){
            System.out.println(((Media)it.next()).getTitle());
        }

        System.out.println("After: ");

        Collections.sort((List)collection);

        it = collection.iterator();

        while(it.hasNext()){
            System.out.println(((Media)it.next()).getTitle());
        }
    }
}
