package hust.soict.globalict.test.disc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class TestPassingParameter {
    public DigitalVideoDisc digitalVideoDisc;
    TestPassingParameter(DigitalVideoDisc dvd){
        this.digitalVideoDisc = dvd;
    }
    public static void main(String[] args) {
        //TODO - Auto-generated method stub
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
        TestPassingParameter jungleDvdWrapper = new TestPassingParameter(jungleDVD);
        TestPassingParameter cinderellaDvdWrapper = new TestPassingParameter(cinderellaDVD);

        swap(jungleDvdWrapper, cinderellaDvdWrapper);
        System.out.println("Jungle dvd title: " + jungleDvdWrapper.digitalVideoDisc.getTitle());
        System.out.println("Cinderella dvd title: " + cinderellaDvdWrapper.digitalVideoDisc.getTitle());

        changeTitle(jungleDVD, cinderellaDVD.getTitle());
        System.out.println("Jungle dvd title: " + jungleDVD.getTitle());
    }

    public static void swap(TestPassingParameter obj1, TestPassingParameter obj2){
        DigitalVideoDisc tmp = obj1.digitalVideoDisc;
        obj1.digitalVideoDisc = obj2.digitalVideoDisc;
        obj2.digitalVideoDisc = tmp;
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title){
        String oldTitle = dvd.getTitle();
        //dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
