package hust.soict.globalict.aims.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class MyDate {
    private final String[] DAY = {"","first","second","third","fourth","fifth","sixth","seventh","eighth","ninth","tenth",
                                 "eleventh","twelfth","thirteenth","fourteenth","fifteenth","sixteenth","seventeenth","eighteenth","nineteenth","twentieth",
                                 "twenty-first","twenty-second","twenty-third","twenty-fourth","twenty-fifth","twenty-sixth","twenty-seventh","twenty-eighth","twenty-ninth","thirtieth",
                                 "thirty-first"};
    private final String[] MONTH = {"","January","February","March","April","May","June","July","August","September","October","November","December"};
    private final String[] NUMBER_NAMES = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
            "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private final String[] TEN_NAMES = {"", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    private int day;
    private int month;
    private int year;

    public MyDate() {
        LocalDate date = LocalDate.now();
        this.day = date.getDayOfMonth();
        this.month = date.getMonthValue();
        this.year = date.getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date){
        LocalDate d = LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
        this.day = d.getDayOfMonth();
        this.month = d.getMonthValue();
        this.year = d.getYear();
    }

    public MyDate(String day, String month, String year){
        //convert day and month to integer number
        this.day = Arrays.asList(DAY).indexOf(day);
        this.month = Arrays.asList(MONTH).indexOf(month);

        //split words in year string and store in names array
        String[] names = year.split(" ");
        //initialize an array list of integer which store numbers
        List<Integer> number = new ArrayList<>();
        //convert each word to integer and store it in number array
        for(String n: names){
            int index = Arrays.asList(TEN_NAMES).indexOf(n);
            if(index>0){
                number.add(index*10);
            }else{
                index = Arrays.asList(NUMBER_NAMES).indexOf(n);
                number.add(index);
            }
        }

        //if the following number of a number which is a multiple of ten is a digit number (1-9)
        //then we add those number and store the sum
        //we use strYear to combine parts of number into a complete number
        StringBuilder strYear = new StringBuilder();
        for(int i=0;i<number.size()-1;i++){
            int x = number.get(i), y = number.get(i+1);
            if(x%10==0 && y<10){
                strYear.append(x+y);
                i++;
            }else{
                strYear.append(x);
            }
        }

        //convert string to int (year)
        this.year = Integer.parseInt(strYear.toString());
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if(day>0 && day <= 31)
            this.day = day;
        else
            System.out.println("Invalid day");
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month>0 && month < 13)
            this.month = month;
        else
            System.out.println("Invalid month");
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void accept(){
        System.out.println("Enter the date with the format: \"yyyy-mm-dd\" with y is year, m is month, d is day");
        Scanner in = new Scanner(System.in);
        String date = in.nextLine();
        LocalDate d = LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
        this.day = d.getDayOfMonth();
        this.month = d.getMonthValue();
        this.year = d.getYear();
    }

    public void print(){
        //add postfix to day number
        String sDay = switch (this.day % 10) {
            case 1 -> "st";
            case 2 -> "nd";
            case 3 -> "rd";
            default -> "th";
        };

        if(this.day<20 && this.day>10) sDay = "th";
        System.out.println(MONTH[this.month]+" "+ this.day + sDay + " " +this.year);
    }

    public void printWithFormat(){
        //let user choose a format
        System.out.println("Choose a format: ");
        Scanner sc = new Scanner(System.in);
        String format = sc.nextLine();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        //set date to Calendar type with values of day, month, year
        Calendar date = Calendar.getInstance();

        //because calendar.month start with 0, we need to subtract this.month by 1
        date.set(this.year, this.month-1, this.day);

        //convert date to format string
        String formatted = dateFormat.format(date.getTime());

        //print out the formatted date
        System.out.println(formatted);
    }
}
