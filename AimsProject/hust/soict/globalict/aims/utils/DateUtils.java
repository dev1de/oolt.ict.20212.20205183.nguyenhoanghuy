package hust.soict.globalict.aims.utils;

import java.util.Arrays;
import java.util.Calendar;

public class DateUtils {
    /*
    @param date1, date2     2 dates need to compare
    @return 0 if 2 dates are equal
            -1 if date1 < date2
            1 if date1 > date2
     */
    public static int compareTwoDates(MyDate date1, MyDate date2) {
        Calendar d1 = Calendar.getInstance(), d2 = Calendar.getInstance();
        d1.set(date1.getYear(),date1.getMonth()-1, date1.getDay());
        d2.set(date2.getYear(), date2.getMonth()-1, date2.getDay());
        return d1.compareTo(d2);
    }

    public static void sortDates(MyDate...dates){
        Arrays.sort(dates, DateUtils::compareTwoDates);
    }
}
