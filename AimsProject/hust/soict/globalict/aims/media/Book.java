package hust.soict.globalict.aims.media;

import java.util.*;

public class Book extends Media implements Comparable<Media>{
    private List<String> authors = new ArrayList<>();
    private String content;
    private List<String> contentTokens;
    private Map<String, Integer> wordFrequency;


    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title, category);
    }

    public Book(int id, String title, String category, List<String> authors, float cost){
        super(title, category);
        this.authors = authors;
        this.cost = cost;
        this.id = id;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setContent(String content){
        this.content = content;
        processContent();
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void addAuthors(String authorName){
        if(!authors.contains(authorName)){
            authors.add(authorName);
            System.out.println("Author " + authorName + " has been added");
        }else{
            System.out.println("Author " + authorName + " is existed in the authors list!");
        }
    }

    public void removeAuthors(String authorName){
        if(authors.contains(authorName)){
            authors.remove(authorName);
            System.out.println("Author " + authorName + " has been removed!");
        }else{
            System.out.println("Author " + authorName + " not found in the authors list!");
        }
    }

    public void processContent(){
        contentTokens = Arrays.asList(content.split(" "));
        Collections.sort(contentTokens);
        wordFrequency = new TreeMap<>();
        for (String word: contentTokens){
            wordFrequency.put(word, wordFrequency.getOrDefault(word,1)+1);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Title: ").append(this.getTitle()).append("\n")
                .append("Category: ").append(this.getCategory()).append("\n")
                .append("Authors: ");
        this.getAuthors().forEach((author) -> {stringBuilder.append(String.format("%s; ",author));});
        stringBuilder.append("\n");
        stringBuilder.append("Content length: ").append(this.wordFrequency.size())
                .append("\nToken:    ");
        wordFrequency.keySet().forEach((word)->{stringBuilder.append(String.format("%10s",word));});
        stringBuilder.append("\n");
        stringBuilder.append("Frequency:");
        wordFrequency.values().forEach((value)->{stringBuilder.append(String.format("%10d",value));});
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    @Override
    public int compareTo(Media o) {
        return this.getTitle().compareTo(o.getTitle());
    }
}
