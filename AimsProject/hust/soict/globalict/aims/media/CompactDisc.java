package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class CompactDisc extends Disc implements Playable, Comparable<Media>{
    private String artist;
    private List<Track> tracks = new ArrayList<>();

    public CompactDisc(String title, String artist) {
        super(title);
        this.artist = artist;
    }

    public CompactDisc(String title, String category, String artist) {
        super(title, category);
        this.artist = artist;
    }

    public CompactDisc(String title, String category, String director, String artist) {
        super(title, category, director);
        this.artist = artist;
    }

    public CompactDisc(int id, String title, String category, String director, String artist, float cost) {
        super(title, category, director, cost);
        this.artist = artist;
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void addTrack(Track track){
        if(tracks.contains(track)){
            System.out.println("Track existed!");
        }else{
            tracks.add(track);
            System.out.println("Track added!");
        }
    }

    public void removeTrack(Track track){
        if(tracks.contains(track)){
            tracks.remove(track);
            System.out.println("Track removed!");
        }else{
            System.out.println("Track not existed!");
        }
    }

    public int getLength(){
        int sum=0;
        for(Track track: tracks){
            sum+=track.getLength();
        }
        this.length = sum;
        return sum;
    }

    @Override
    public void play() {
        System.out.println("Playing CD: " + this.getTitle());
        System.out.println("Artist: " + this.getArtist());
        System.out.println("Tracks: ");
        for(Track track: tracks){
            track.play();
            System.out.println();
        }
    }

    public int compareTo(DigitalVideoDisc o){
        return this.getTitle().compareTo(o.getTitle());
    }

    public int compareTo(Book o){
        return this.getTitle().compareTo(o.getTitle());
    }

    public int compareTo(CompactDisc o) {
        return Integer.compare(this.tracks.size(),((CompactDisc) o).tracks.size());
    }

    @Override
    public int compareTo(Media o) {
        if(o.getClass()!=this.getClass()) return this.getTitle().compareTo(o.getTitle());
        return Integer.compare(this.tracks.size(),((CompactDisc) o).tracks.size());
    }
}
