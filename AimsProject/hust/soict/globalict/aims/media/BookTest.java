package hust.soict.globalict.aims.media;

import java.util.Arrays;

public class BookTest {
    public static void main(String[] args) {
        Book b1 = new Book(1,"Harry Potter","Sci-fi", Arrays.asList("J.K.Rowling","J.k","Rowling","jkrowl"),4.5f);
        b1.setContent("Do not pity the dead Harry Pity the living and above all those who live without love");
        System.out.println(b1.toString());
    }
}
