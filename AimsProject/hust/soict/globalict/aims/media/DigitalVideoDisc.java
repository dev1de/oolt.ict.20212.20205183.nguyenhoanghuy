package hust.soict.globalict.aims.media;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<Media>{



    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }
    public DigitalVideoDisc(String title, String category, String director) {
        super(title, category, director);
    }

    public DigitalVideoDisc(int id, String title, String category, String director, int length, float cost) {
        super(title, category, director, cost);
        this.length = length;
        this.id = id;
    }



    public boolean search(String title){
        //split title of the object and the search title into tokens
        List<String> thisTitle = new ArrayList<>(Arrays.asList(this.getTitle().split(" ")));
        String[] checkTitle = title.split(" ");

        //check if each token in the search title is in object's title or not
        //the function return false if at least 1 token not found
        //otherwise return true
        for (String token: checkTitle) {
            if(!thisTitle.contains(token)){
                return false;
            }
        }
        return true;
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }


    @Override
    public int compareTo(Media o) {
        if(o.getClass()!=this.getClass()) return this.getTitle().compareTo(o.getTitle());
        return Float.compare(this.getCost(), o.getCost());
    }
}
