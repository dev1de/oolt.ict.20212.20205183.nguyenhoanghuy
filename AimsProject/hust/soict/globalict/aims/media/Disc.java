package hust.soict.globalict.aims.media;

public class Disc extends Media {
    protected int length;
    private String director;

    public Disc(String title) {
        super(title);
    }

    public Disc(String title, String category) {
        super(title, category);
    }

    public Disc(String title, String category, String director) {
        super(title, category);
        this.director = director;
    }


    public Disc(String title, String category, String director, float cost) {
        super(title, category);
        this.cost = cost;
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public String getDirector() {
        return director;
    }


}
