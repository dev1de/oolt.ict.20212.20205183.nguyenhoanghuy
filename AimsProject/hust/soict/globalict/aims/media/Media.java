package hust.soict.globalict.aims.media;

public class Media{
    private String title;
    private String category;
    protected float cost;
    protected int id;

    public Media(String title) {
        this.title = title;
    }

    public Media(String title, String category) {
        this.title = title;
        this.category = category;
    }


    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }

    @Override
    public boolean equals(Object o){
        // return false if o is not an object of Media class
        return  ((Media)o).id == id;
    }


}
