package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

import java.util.ArrayList;
import java.util.List;

public class Order {
    public static final int MAX_NUMBER_ORDER = 10;
    private final MyDate dateOrdered;
    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders = 0;
    private List<Media> itemsOrdered = new ArrayList<>();

    public Order(){
        dateOrdered = new MyDate();
        if(nbOrders< MAX_LIMITED_ORDERS){
            nbOrders += 1;
        }else{
            throw new Error("The number of orders is full");
        }
    }

    public void addMedia(Media ...media){
        for (Media m : media) {
            if(itemsOrdered.contains(m)){
                System.out.println("Media " + m.getTitle() + " is existed!");
            }else if(itemsOrdered.size()>=MAX_NUMBER_ORDER){
                System.out.println("Max number of medias reached!");
                return;
            }
            else{
                itemsOrdered.add(m);
                System.out.println("Media " + m.getTitle() + " is added");
            }
        }
    }

    public void removeMedia(Media ...media){
        for(Media m : media){
            if(itemsOrdered.contains(m)){
                itemsOrdered.remove(m);
                System.out.println("Media " + m.getTitle() + " is removed");
            }else{
                System.out.println("Media " + m.getTitle() + " is not existed!");
            }
        }
    }

    

    public void printOrder(){
        System.out.println("***********************hust.soict.globalict.aims.order.Order***********************");
        System.out.print("Date: ");
        dateOrdered.print();
        for(int i=0;i<itemsOrdered.size();i++){
            System.out.println((i+1)+". DVD - " +
                                itemsOrdered.get(i).getTitle() + " - " +
                                itemsOrdered.get(i).getCategory() + " - " +
                                //itemsOrdered.get(i).getDirector() + " - " +
                                /*itemsOrdered.get(i).getLength() +*/ ": $" +
                                itemsOrdered.get(i).getCost());
        }
        System.out.println("Total cost: $" + totalCost());
        System.out.println("***************************************************");
    }


    public float totalCost(){
        float sum=0;
        for (Media media : itemsOrdered) {
            sum += media.getCost();
        }
        return sum;
    }

    public Media getALuckyItem(){
        int randomNumber = (int)(Math.random()*itemsOrdered.size());
        Media luckyMedia = itemsOrdered.get(randomNumber);
        //itemsOrdered.get(randomNumber).setCost(0);
        return luckyMedia;
    }

}
