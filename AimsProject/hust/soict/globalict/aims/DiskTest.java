package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {
    public static void main(String[] args) {
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Harry fucking Potter and your ass");
        //dvd1.setCost(15);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("John Wick 2");
        //dvd2.setCost(30);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("The Dark Phoenix");
        //dvd3.setCost(23);

        Order newOrder = new Order();
        newOrder.addMedia(dvd1, dvd2, dvd3);

        System.out.println(dvd1.search("Harry Potter"));
        System.out.println(dvd2.search("Wick 2"));
        System.out.println(newOrder.getALuckyItem().getTitle());
        newOrder.printOrder();
    }
}
