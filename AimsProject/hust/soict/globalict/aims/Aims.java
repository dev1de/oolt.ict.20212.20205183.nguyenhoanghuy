package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Aims {
    private static Order myOrder;

    public static void createOrder(){
        myOrder = new Order();
        System.out.println("New order created!");
    }

    public static void addItem(){
        int option;
        String title, category;
        float cost;

        System.out.println("Which type of item do you want to add?");
        System.out.println("1. Book");
        System.out.println("2. CD");
        System.out.println("3. DVD");
        Scanner sc = new Scanner(System.in);
        option = sc.nextInt();

        System.out.println("Enter ID: ");
        int id = sc.nextInt();
        System.out.println("Enter title");
        title = sc.nextLine();
        System.out.println("Enter category");
        category = sc.nextLine();

        switch (option){
            case 1:
                System.out.println("Enter authors: ");
                String readLine = sc.nextLine();
                List<String> authors = Arrays.asList(readLine.split(","));
                System.out.println("Enter cost: ");
                cost = sc.nextFloat();
                Book book = new Book(id, title, category, authors, cost);

                myOrder.addMedia(book);
                break;
            case 2:
                System.out.println("Enter artist: ");
                String artist = sc.nextLine();
                System.out.println("Enter director: ");
                String director = sc.nextLine();
                System.out.println("Enter price: ");
                cost = sc.nextFloat();
                CompactDisc cd = new CompactDisc(id, title, category, artist, director, cost);
                System.out.println("Enter track list: ");
                System.out.println("How many tracks in the list?");
                int trackNums = sc.nextInt();
                for(int i=0; i<trackNums;i++) {
                    System.out.println("\tEnter track: ");
                    System.out.println("\tTitle: ");
                    String trackTitle = sc.nextLine();
                    System.out.println("\tLength: ");
                    int length = sc.nextInt();
                    Track newTrack = new Track(trackTitle, length);
                    cd.addTrack(newTrack);
                }
                System.out.println("CD : " + cd.getTitle());
                System.out.println("Total length: " + cd.getLength());
                System.out.println("Do you want to play the CD? (Y/N)");
                String play = sc.nextLine();
                if(play.equals("Y")){
                    cd.play();
                }
                myOrder.addMedia(cd);
                break;
            case 3:
                System.out.println("Enter director: ");
                director = sc.nextLine();
                System.out.println("Enter length: ");
                int length = sc.nextInt();
                System.out.println("Enter price: ");
                cost = sc.nextFloat();
                DigitalVideoDisc dvd = new DigitalVideoDisc(id,title, category, director, length, cost);
                myOrder.addMedia(dvd);
                System.out.println("DVD : " + dvd.getTitle());
                System.out.println("Length : " + dvd.getLength());
                System.out.println("Do you want to play this DVD? (Y/N)");
                play = sc.nextLine();
                if(play.equals("Y")){
                    dvd.play();
                }
                break;
            default:
                System.out.println("Invalid option!");
                break;
        }
    }

    public static void removeId(){
        System.out.println("Enter ID: ");
        Scanner sc = new Scanner(System.in);
        int id = sc.nextInt();
        myOrder.removeMedia();
    }


    public static void showMenu(){
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void main(String[] args){
        showMenu();
        //memory daemon thread
        Thread memoryDaemon =new Thread(new MemoryDaemon());
        memoryDaemon.setDaemon(true);
        memoryDaemon.start();

        /*
        Order anOrder = new Order();
        //create a new dvd

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Rogers Allers");
        dvd1.setLength(87);
        //add dvd1 to order
        anOrder.addMedia(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas");
        dvd2.setCost(24.95f);
        dvd2.setLength(124);
        anOrder.addMedia(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin","Animation","John Musker", 90, 18.99f);
        anOrder.addMedia(dvd3);

        System.out.println("Total cost is: " + anOrder.totalCost());

        System.out.println("Remove \"The Lion King\"");
        //remove the dvd1
        anOrder.removeMedia(dvd1);

        System.out.println("Total cost after remove: " + anOrder.totalCost());

        Order myOrder = new Order();
        myOrder.addMedia(dvd1,dvd2);
        myOrder.printOrder();
        anOrder.printOrder();
        */



    }
}
