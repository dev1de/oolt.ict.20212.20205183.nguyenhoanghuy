package hust.soict.globalict.lab02;

import java.util.Arrays;

public class ArraySortSum {
    //constant array
    private static final int[] ARRAY = {1789, 2035, 2219, 1510, 1249, 304, 1294, 2913};
    public static void main(String[] args){
        //original array
        System.out.println(Arrays.toString(ARRAY));

        //sort array with Arrays.sort()
        Arrays.sort(ARRAY);
        System.out.println("Array after sort: " + Arrays.toString(ARRAY));

        //calculate sum of the array
        int sum=0;
        for(int num:ARRAY){
            sum += num;
        }
        System.out.println("Sum of the array: " + sum);
    }
}
