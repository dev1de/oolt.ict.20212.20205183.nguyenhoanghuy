package hust.soict.globalict.lab02;

import java.util.Objects;
import java.util.Scanner;

public class DaysOfAMonth {
    //valid input for each month
    public static final String[][] MONTH = {{"January","Jan","Jan.","1"},
                                            {"February","Feb","Feb.","2"},
                                            {"March","Mar","Mar.","3"},
                                            {"April","Apr","Apr","4"},
                                            {"May","May","May.","5"},
                                            {"June","Jun","Jun.","6"},
                                            {"July","Jul","Jul.","7"},
                                            {"August","Aug","Aug.","8"},
                                            {"September","Sep","Sep.","9"},
                                            {"October","Oct","Oct.","10"},
                                            {"November","Nov","Nov.","11"},
                                            {"December","Dec","Dec.","12"}};

    /**
     *
     * @param month input string entered by user
     * @return      month in integer which matches the input, e.g. return 3 when input equal "Mar", return -1 if the input is invalid
     */
    public static int MonthNumber(String month){
        for(String[] m:MONTH){
            for(String name:m){
                if(Objects.equals(month, name)){
                    return Integer.parseInt(m[m.length-1]);
                }
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int iYear, iMonth, days;
        String yearType, strMonth;
        //enter year from keyboard until year is valid(>0)
        do {
            System.out.println("Enter year");
            iYear = sc.nextInt();
        }while (iYear<0);

        //check if that year is a leap year or a common year
        if(iYear%4==0){
            if(iYear%100==0 && iYear%400!=0){
                yearType = "Common year";
            }else{
                yearType = "Leap year";
            }
        }else{
            yearType = "Common year";
        }

        //input buffer
        sc.nextLine();

        //enter month until month is valid (check by MonthNumber)
        do {
            System.out.println("Enter month");
            strMonth = sc.nextLine();
            iMonth = MonthNumber(strMonth);
        }while(iMonth==-1);

        //determine number of days in that month and year
        days = switch (iMonth) {
            case 1, 3, 5, 7, 8, 10, 12 -> 31;
            case 4, 6, 9, 11 -> 30;
            case 2 -> yearType.equals("Leap year") ? 29 : 28;
            default -> 0;
        };

        //print out the result
        System.out.println(strMonth + " of " + iYear + " has " + days + " days" );
    }
}
