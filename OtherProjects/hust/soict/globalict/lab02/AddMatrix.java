package hust.soict.globalict.lab02;

import java.util.Arrays;

public class AddMatrix {
    private static final int[][] MATRIX_A = {{1, 2, 3},
                                            {8, 4, 5},
                                            {6,7,9}};
    private static final int[][] MATRIX_B = {{10,23,12},
                                            {5,6,7},
                                            {1,92, 21}};

    public static void main(String[] args){
        int[][] matrixSum = new int[MATRIX_A.length][MATRIX_A[0].length];

        //original two matrices
        System.out.println("Matrix A: " + Arrays.deepToString(MATRIX_A));
        System.out.println("Matrix B: " + Arrays.deepToString(MATRIX_B));

        //add each element in matrix a with corresponding element in matrix b, store the result in matrix sum
        for(int i=0;i<MATRIX_A.length;i++){
            for(int j=0; j<MATRIX_A[0].length;j++){
                matrixSum[i][j] = MATRIX_A[i][j] + MATRIX_B[i][j];
            }
        }

        //print out the result
        System.out.println("A + B = " + Arrays.deepToString(matrixSum));
    }
}
