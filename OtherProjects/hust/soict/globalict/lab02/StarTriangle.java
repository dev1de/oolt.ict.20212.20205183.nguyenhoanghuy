package hust.soict.globalict.lab02;

import java.util.Scanner;

public class StarTriangle {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        //enter n
        System.out.println("Enter n");
        int n = sc.nextInt();

        //number of the whitespace before the *
        //number of whitespace start at n-1 whitespace in the first line, the decrease by 1 in the next line
        String whiteSpace = " ".repeat(n-1);
        //number of star
        //it starts at 1 star then increase by 2 in the next line
        String star = "*";

        //print whitespace and star in each line, whitespace = "" when we reach the last line (n-th line)
        //otherwise decrease the number of whitespace by 1 using substring
        for(int i=0; i<n; i++,
                            whiteSpace= i==n-1 ? whiteSpace = "" :whiteSpace.substring(1),
                            star = star.concat("**")){
            System.out.println(whiteSpace + star);
        }
    }
}
