package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class Operation{
	public static void main(String[] args){
		String strNum1, strNum2, quots="";
		double num1, num2;
		strNum1 = JOptionPane.showInputDialog(null, "Input the first number",JOptionPane.INFORMATION_MESSAGE);
		strNum2 = JOptionPane.showInputDialog(null, "Input the second number",JOptionPane.INFORMATION_MESSAGE);
		num1 = Double.parseDouble(strNum1);
		num2 = Double.parseDouble(strNum2);
		double sum, diff, prod, quot;
		sum = num1 + num2;
		diff = num1 - num2;
		prod = num1 * num2;
		if(num2!=0){
			quot = num1/num2;
			quots = quots + quot;
		}else {
			quots = "Invalid operation";
		}
		JOptionPane.showMessageDialog(null,"Sum: " + sum +
						   "\nDifference: " + diff + 
						   "\nProduct: " + prod + 
						   "\nQuotient: " +quots);
	}
}
