package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
import java.lang.Math;

public class Equations{
	private static final int INFOR_MESS = JOptionPane.INFORMATION_MESSAGE;
	public static String FirstDegreeOneVar(){
		String strA, strB;
		strA = JOptionPane.showInputDialog(null, "Input a", INFOR_MESS);
		strB = JOptionPane.showInputDialog(null, "Input b", INFOR_MESS);
		Double a = Double.parseDouble(strA), b = Double.parseDouble(strB);
		Double x;
		if(a!=0){
			x = -b/a;
			return "The equation has one solution: x = " + x;
		} else{
			if(b!=0) return "The equation has no solution";
			return "The equation has many solutions";
		}
	}
	public static String FirstDegreeTwoVar(){
		String strA11, strA12, strA21, strA22, strB1, strB2;
		strA11 = JOptionPane.showInputDialog(null, "Input a11", INFOR_MESS);
		strA12 = JOptionPane.showInputDialog(null, "Input a12", INFOR_MESS);
		strA21 = JOptionPane.showInputDialog(null, "Input a21", INFOR_MESS);
		strA22 = JOptionPane.showInputDialog(null, "Input a22", INFOR_MESS);
		strB1 = JOptionPane.showInputDialog(null, "Input b1", INFOR_MESS);
		strB2 = JOptionPane.showInputDialog(null, "Input b2", INFOR_MESS);
		Double a11 = Double.parseDouble(strA11),
				 a12 = Double.parseDouble(strA12),
				 a21 = Double.parseDouble(strA21),
				 a22 = Double.parseDouble(strA22),
				 b1 = Double.parseDouble(strB1),
				 b2 = Double.parseDouble(strB2);
		Double d = a11*a22 - a21*a12,
				 d1 = b1*a22 - b2*a12,
				 d2 = b2*a11 - b1*a21;
		Double x1,x2;
		if(d!=0){
			x1 = d1/d;
			x2 = d2/d;
			return "The system has a unique solution (x1,x2) = (" + x1 + ", " + x2 + ")";
		}else{
			if(d1==0 && d2==0)
				return "The system has infinitely many solutions";
			return "The system has no solution";
		}
	}
	public static String SecDegreeOneVar(){
		String strA, strB, strC;
		strA = JOptionPane.showInputDialog(null, "Input a", INFOR_MESS);
		strB = JOptionPane.showInputDialog(null, "Input b", INFOR_MESS);
		strC = JOptionPane.showInputDialog(null, "Input c", INFOR_MESS);
		Double a = Double.parseDouble(strA),
				 b = Double.parseDouble(strB),
				 c = Double.parseDouble(strC);
		Double delta = b*b-4*a*c;
		if (delta==0){
			Double x = -b/(2*a);
			return "The equation has double root x = " + x;
		}else if(delta > 0){
			Double x1 = (-b + Math.sqrt(delta))/(2*a), x2 = (b + Math.sqrt(delta))/(2*a);
			return "The equation has two distinct root x1 = " + x1 + ", x2 = " + x2;
		}else{
			return "The equation has no solution";
		}
	}
	public static void main(String[] args){
		String option = JOptionPane.showInputDialog(null, 
																  "Choose your type of equation:"+
																  "\n1. First-degree equation with one variable"+
																  "\n2. System of first-degree equation with two variables"+
																  "\n3. Second-degree equation with one variable",
																  INFOR_MESS);
		Integer userOption = Integer.parseInt(option);
 		String notification;
		switch(userOption){
			case 1:
				notification = FirstDegreeOneVar();
				break;
			case 2:
				notification = FirstDegreeTwoVar();
				break;
			case 3:
				notification = SecDegreeOneVar();
				break;
			default:
				notification = "Invalid Option";
		}
		JOptionPane.showMessageDialog(null, notification, "Equation Result", INFOR_MESS);
	} 
}
